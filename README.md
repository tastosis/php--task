Деплой
=====================================

- загрузка проекта

git clone git@gitlab.com:tastosis/php-test-task.git

- импорт базы данных (выполнить из корневого каталога проекта)

mysql -u %username% -p < db.sql

- добавление трейлеров (выполнить из корневого каталога проекта)

php bin/console fetch:trailers

- запуск тестов

php vendor/bin/phpunit

Я запускал приложение через nginx, примерный конфиг для nginx лежит в папке nginx_config (актуален для дефолтной инсталляции nginx под ubuntu 20.04 LTS. root и путь к php-fpm сокету надо обновить для своего тестового стенда. Можно, конечно, запускать и через php сервер.

Необходимо прописать актуальные данные в переменнной DATABASE в .env



Комментарии
--------------------

- Выполните все этапы для middle;
  
Выполнил

- предложите другую схему получения rss и преобразования полученного в итоговый класс;
  
Вместо SimpleXML можно использовать DOM, xml_parser, XMLReader. Самый быстрый (и самый сложный в реализации) из них это xml_parser. Если взять готовые фреймворки, то на питоне есть неплохое решение Scrapy, которое в том числе умеет парсить xml (мне приходилось его использовать на текущей работе)

- предожите схему валидации полей сущностей;
  
Не совсем понятно что имеется в виду. Если под сущностями подразумевается распарсенные xml поля, то их можно проверять по регулярным выражениям. Если поля в базе - так ORM сама не даст прогрузить что то, что не соответствует их типам, можно повесить отдельный эксепшн на запись в базу. В общем случае для валидации любых полей на уровне сервера можно использовать пакет cakephp/validation

- добавьте пользователей;
  
Добавил 3х пользователей (логин / пароль)

michael Mciaine18735

john IamThedeadman1995

scarlett Person@lNightmare11

- реализуйте отметки «Нравится» для загруженных трейлеров;
  
Сделал

- напишите тесты;

Сделал несколько тестов для написанных контроллеров

Касаемо ошибок - к сожалению в силу загруженности по основной работе всю неделю у меня было не так много времени ознакомиться с архитектурой предложенного фреймворка, так что совсем откровенных ошибок я не увидел. Разве что убрал неиспользуемые импорты и переменные. Еще заметил что неймспейс ConfigTest.php не соответствует PSR-4 стандарту, но это не принципиально. Туда же я поместил свои тесты.

По поводу предложений по тесту - я бы добавил задачу реализации лайков на вебсокетах, чтобы они обновлялись в реальном времени (как в фейсбуке/вконтакте)