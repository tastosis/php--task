<?php

namespace Tests;

use PHPUnit\Framework\TestCase;
use Tests\Traits\AppTestTrait;

class UserControllerTest extends TestCase
{
    use AppTestTrait;

    public function testLoginAction(): void
    {
        $request = $this->createJsonRequest('POST', '/login', ['login' => 'aaa', 'password' => 'bbb']);
        $response = $this->app->handle($request);
        $body = $response->getBody()->__toString();
        self::assertSame(200, $response->getStatusCode());
        self::assertNotEmpty($body);
        self::assertJson($body);
    }

    public function testLogoutAction(): void
    {
        $request = $this->createJsonRequest('POST', '/logout', []);
        $response = $this->app->handle($request);
        $body = $response->getBody()->__toString();
        self::assertSame(200, $response->getStatusCode());
        self::assertNotEmpty($body);
        self::assertJson($body);
        self::assertEquals('{"success":0}', $body);
    }

    public function testSuccessLogoutAction(): void
    {
        $request = $this->createJsonRequest('POST', '/logout', ['logout' => '1']);
        $response = $this->app->handle($request);
        $body = $response->getBody()->__toString();
        self::assertSame(200, $response->getStatusCode());
        self::assertNotEmpty($body);
        self::assertJson($body);
        self::assertEquals('{"success":1}', $body);
    }
}