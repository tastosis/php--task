<?php

namespace Tests;

use PHPUnit\Framework\TestCase;
use Tests\Traits\AppTestTrait;

class LikesControllerTest extends TestCase
{
    use AppTestTrait;

    public function testLikeAction(): void
    {
        $id = $this->getMinTrailerId();
        $request = $this->createJsonRequest('POST', '/trailers/like', ['id' => $id]);
        $response = $this->app->handle($request);
        $body = $response->getBody()->__toString();
        self::assertSame(200, $response->getStatusCode());
        self::assertNotEmpty($body);
        self::assertJson($body);

    }
}