<?php

namespace Tests;

use PHPUnit\Framework\TestCase;
use Tests\Traits\AppTestTrait;

class TrailerControllerTest extends TestCase
{
    use AppTestTrait;

    public function testDetailAction(): void
    {
        $id = $this->getMinTrailerId();
        $request = $this->createRequest('GET', '/trailers/' . $id);
        $response = $this->app->handle($request);
        $body = $response->getBody()->__toString();
        self::assertSame(200, $response->getStatusCode());
        self::assertNotEmpty($body);
        self::assertGreaterThan(0, strpos($body, '<article>'));
    }
}