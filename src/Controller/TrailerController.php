<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Movie;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Exception\HttpBadRequestException;
use Twig\Environment;

/**
 * Class TrailerController.
 */
class TrailerController
{
    /**
     * @var Environment
     */
    private $twig;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * TrailerController constructor.
     *
     * @param Environment             $twig
     * @param EntityManagerInterface  $em
     */
    public function __construct(Environment $twig, EntityManagerInterface $em)
    {
        $this->twig = $twig;
        $this->em = $em;
    }

    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     *
     * @return ResponseInterface
     *
     * @throws HttpBadRequestException
     */
    public function detail(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        $trailer = $this->em->getRepository(Movie::class)->findOneBy(['id' => $request->getAttribute('id')]);
        $datetime = new \DateTime("now", new \DateTimeZone('Europe/Moscow'));
        $datetime = $datetime->format('d.m.Y H:i') . ' MSK';
        $user_id = UserController::getAuthorizedUserId();
        $authorized = false;
        if ($user_id !== null) {
            $authorized = true;
            $user = $this->em->getRepository(User::class)->findOneBy(['id' => $user_id]);
        }
        try {
            $params = [
                'current_datetime' => $datetime,
                'class' => static::class,
                'method' => str_replace(static::class . '::', '', __METHOD__),
                'trailer' => $trailer,
                'authorized' => $authorized
            ];
            if ($authorized) {
                $params['full_name'] = $user->getFullName();
                $params['avatar'] = $user->getAvatar();
            }
            $data = $this->twig->render('home/detail.html.twig', $params);
        } catch (\Exception $e) {
            throw new HttpBadRequestException($request, $e->getMessage(), $e);
        }

        $response->getBody()->write($data);
        return $response;
    }
}