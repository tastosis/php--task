<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Movie;
use App\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Exception\HttpBadRequestException;
use Twig\Environment;

/**
 * Class HomeController.
 */
class HomeController
{
    /**
     * @var Environment
     */
    private $twig;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * HomeController constructor.
     *
     * @param Environment             $twig
     * @param EntityManagerInterface  $em
     */
    public function __construct(Environment $twig, EntityManagerInterface $em)
    {

        $this->twig = $twig;
        $this->em = $em;
    }

    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface      $response
     *
     * @return ResponseInterface
     *
     * @throws HttpBadRequestException
     */
    public function index(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        $datetime = new \DateTime("now", new \DateTimeZone('Europe/Moscow'));
        $datetime = $datetime->format('d.m.Y H:i') . ' MSK';
        $user_id = UserController::getAuthorizedUserId();
        $authorized = false;
        if ($user_id !== null) {
            $authorized = true;
            $user = $this->em->getRepository(User::class)->findOneBy(['id' => $user_id]);
        }
        try {
            $params = [
                'trailers' => $this->fetchData(),
                'current_datetime' => $datetime,
                'class' => static::class,
                'method' => str_replace(static::class . '::', '', __METHOD__),
                'authorized' => $authorized
            ];
            if ($authorized) {
                $params['full_name'] = $user->getFullName();
                $params['avatar'] = $user->getAvatar();
            }
            $data = $this->twig->render('home/index.html.twig', $params);
        } catch (\Exception $e) {
            throw new HttpBadRequestException($request, $e->getMessage(), $e);
        }

        $response->getBody()->write($data);

        return $response;
    }

    /**
     * @return Collection
     */
    protected function fetchData(): Collection
    {
        $data = $this->em->getRepository(Movie::class)
            ->findAll();

        return new ArrayCollection($data);
    }
}