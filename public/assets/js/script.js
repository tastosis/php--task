$(document).ready(function() {

    let showPlate = (plate_id) => {
        let timeout,
            current_plate = $('#' + plate_id + '-plate');
        $('.alert').hide();
        current_plate.fadeIn('fast');
        clearTimeout(timeout);
        timeout = setTimeout(() => {
            current_plate.fadeOut('fast');
        }, 3000)
    }

    $('.container').on('click', '.j-like', function() {
        let id = $(this).data('id');
        $.post({
            url: '/trailers/like',
            data: {id: id},
            success: (json) => {
                if (json.success) {
                    $('.likes-count-value').text(json.likes);
                    $('.likes-count').removeClass('invisible');
                } else console.log(json.message)
            }
        });
        return false;
    });

    $('.j-login-form').on('submit', function (e) {
        e.preventDefault();
        let formData = $(this).serialize();
        console.log(formData);
        $.post({
            url: '/login',
            data: formData,
            success: (data) => {
                data.success == 1;
                if (data.success) {
                    showPlate('success')
                    setTimeout(() => {
                        location.reload();
                    }, 1000);
                } else {
                    showPlate('fail')
                }
            },
            error: () => {
                showPlate('error');
            }
        });
        return false;
    });

    $('.navbar').on('click', '.j-logout', function() {
        $.post({
            url: '/logout',
            data: {logout: 1},
            success: (data) => {
                if (data.success) {
                    location.reload();
                } else
                    showPlate('error');
            },
            error: () => {
                showPlate('error');
            }
        });
    });
});