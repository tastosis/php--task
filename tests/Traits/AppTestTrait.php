<?php

namespace Tests\Traits;

use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\UriInterface;
use Slim\App;
use Slim\Interfaces\CallableResolverInterface;
use Slim\Interfaces\RouteCollectorInterface;
use Slim\Interfaces\RouteResolverInterface;
use Slim\Middleware\ErrorMiddleware;
use Slim\Middleware\RoutingMiddleware;
use Slim\Psr7\Factory\ServerRequestFactory;
use UltraLite\Container\Container;
use UnexpectedValueException;

/**
 * App Test Trait.
 */
trait AppTestTrait
{
    /**
     *
     * @var int
     */

    private static $min_trailer_id = 1;

    /**
     *
     * @var Container
     */
    protected $container;

    /**
     * @var App
     */
    protected $app;

    /**
     * setup bootstrap
     *
     * @throws UnexpectedValueException
     *
     * @return void
     */
    protected function setUp(): void
    {
        $container = require __DIR__ . '/../../bootstrap.php';
        if ($container === null) {
            throw new UnexpectedValueException('Container must be initialized');
        }

        $this->container = $container;
        $this->app = new App(
            $container->get(ResponseFactoryInterface::class),
            $container,
            $container->get(CallableResolverInterface::class),
            $container->get(RouteCollectorInterface::class),
            $container->get(RouteResolverInterface::class)
        );

        $this->app->add($container->get(RoutingMiddleware::class));
        $this->app->add($container->get(ErrorMiddleware::class));
    }

    protected function getMinTrailerId() {
        return self::$min_trailer_id;
    }

    /**
     *
     * @param string $method The HTTP method
     * @param string|UriInterface $uri The URI
     * @param array $serverParams The server parameters
     *
     * @return ServerRequestInterface
     */
    protected function createRequest(
        string $method,
        $uri,
        array $serverParams = []
    ): ServerRequestInterface {
        return (new ServerRequestFactory())->createServerRequest($method, $uri, $serverParams);
    }

    /**
     *
     * @param string $method The HTTP method
     * @param string|UriInterface $uri The URI
     * @param array|null $data The json data
     *
     * @return ServerRequestInterface
     */
    protected function createJsonRequest(
        string $method,
        $uri,
        array $data = null
    ): ServerRequestInterface {
        $request = $this->createRequest($method, $uri);

        if ($data !== null) {
            $request = $request->withParsedBody($data);
        }
        return $request->withHeader('Content-Type', 'application/json');
    }
}