<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Exception\HttpBadRequestException;

/**
 * Class UserController.
 */
class UserController
{

    private const SALT = '7UiIaP10zKlaMn8018sQz==';

    /**
     * @var EntityManagerInterface
     */
    private $doctrine;

    /**
     * UserController constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->doctrine = $em;
    }

    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     *
     * @return ResponseInterface
     *
     */
    public function login(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        $credentials = (array)$request->getParsedBody();
        $credentials['password'] = \sha1($credentials['password'] . self::SALT);
        try {
            $user = $this->doctrine->getRepository(User::class)->findOneBy(['login' => $credentials['login'], 'password' => $credentials['password']]);
            if ($user === null) {
                $success = 0;
            } else {
                $this->authorize($user);
                $success = 1;
            }
        } catch (\Exception $e) {
            throw new HttpBadRequestException($request, $e->getMessage(), $e);
        }

        $response->getBody()->write(json_encode(['success' => $success]));
        return $response->withHeader('Content-Type', 'application/json; charset=utf-8');
    }

    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     *
     * @return ResponseInterface
     *
     */
    public function logout(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        $body = $request->getParsedBody();
        if (isset($body['logout']) && $body['logout'] == '1') {
            @\setcookie("test-app-auth", "", \time() - 3600 * 24);
            @\setcookie("test-app-auth-id", "", \time() - 3600 * 24);
            $success = 1;
        } else {
            $success = 0;
        }
        $response->getBody()->write(json_encode(['success' => $success]));
        return $response->withHeader('Content-Type', 'application/json; charset=utf-8');
    }

    /**
     *
     * Function for user authorization in the application
     *
     * @param User $user
     *
     * @return void
     *
     */
    private function authorize(User $user): void
    {
        $id = $user->getId();
        if ($id > 0) { //additional check, it is not necessary
            $value = \sha1($id . self::SALT);
            \setcookie("test-app-auth", $value, \time() + 3600 * 24 * 7);
            \setcookie("test-app-auth-id", (string)$id, \time() + 3600 * 24 * 7);
        }
    }

    /**
     * Function to check user authorization in the application
     *
     * @return int|null
     *
     */
    public static function getAuthorizedUserId(): ?int
    {
        if (isset($_COOKIE['test-app-auth-id']) && isset($_COOKIE['test-app-auth'])) {
            $user_id = (int)$_COOKIE['test-app-auth-id'];
            if ($user_id > 0 && $_COOKIE['test-app-auth'] && sha1($user_id . self::SALT) === $_COOKIE['test-app-auth']) {
                return $user_id;
            }
        }
        return null;
    }
}