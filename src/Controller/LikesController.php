<?php

declare(strict_types=1);

namespace App\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Exception\HttpBadRequestException;


/**
 * Class LikesController.
 */
class LikesController
{

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * LikesController constructor.
     *
     * @param EntityManagerInterface  $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     *
     * @return ResponseInterface
     *
     * @throws HttpBadRequestException
     */
    public function like(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {

        try {
            $trailer_id = (int)$request->getParsedBody()['id'];
            $db = $this->em->getConnection();
            $stmt = $db->prepare("update movie set likes = likes + 1 where id = ?");
            $stmt->execute([$trailer_id]);

            $stmt = $db->prepare("select likes from movie where id = ?"); //this is in case when someone else liked this
            $stmt->execute([$trailer_id]);
            $row = $stmt->fetch(\PDO::FETCH_ASSOC);
            $data = json_encode(['success' => 1, 'likes' => $row['likes']]);
        } catch (\Exception $e) {
            throw new HttpBadRequestException($request, $e->getMessage(), $e);
        }

        $response->getBody()->write($data);
        return $response->withHeader('Content-Type', 'application/json; charset=utf-8');
    }
}