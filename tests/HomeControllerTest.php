<?php

namespace Tests;

use PHPUnit\Framework\TestCase;
use Tests\Traits\AppTestTrait;

class HomeControllerTest extends TestCase
{
    use AppTestTrait;

    public function testIndexAction(): void
    {
        $request = $this->createRequest('GET', '/');
        $response = $this->app->handle($request);
        $body = $response->getBody()->__toString();
        self::assertSame(200, $response->getStatusCode());
        self::assertNotEmpty($body);
        self::assertGreaterThan(0, strpos($body, '<h1 class="mt-5 mb-4">'));
    }
}