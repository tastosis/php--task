drop database if exists slim;
create database slim;
use slim;
create table movie (
    id int not null auto_increment,
    title varchar(255),
    link text,
    description text,
    pub_date datetime,
    image varchar(255),
    likes integer default 0,
    primary key (id),
    key (title)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

create table user (
    id int not null auto_increment,
    login varchar(20) not null,
    password varchar(100) not null,
    full_name varchar(255) not null,
    avatar varchar(255),
    primary key (id),
    key (login, password)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

insert into user (login, password, full_name, avatar) values ('michael', 'ac0de9dbf9c9f71ffdd91da2560b94e7c6b6e145', 'Michael Caine', '/upload/avatars/mcaine.jpg');
insert into user (login, password, full_name, avatar) values ('john', 'bb80ed4332d92057aa0332d1188e14a6e55dcf58', 'Johnny Depp', '/upload/avatars/ddepp.jpg');
insert into user (login, password, full_name, avatar) values ('scarlett', '49df80bc5282c98b8d03482d3b1da3cd3f099de1', 'Scarlett Johansson', '/upload/avatars/johansson.jpg');